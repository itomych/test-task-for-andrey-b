import React from "react";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Field, Form, Formik} from "formik";
import {TextField} from "formik-material-ui";

const useStyles = makeStyles(theme => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: `1px solid ${theme.palette.primary.main}`,
    borderRadius: 5,
    boxShadow: theme.shadows[1],
    padding: theme.spacing(2, 4, 4),
    outline: "none",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
}));

const ModalComponent = ({open, handleClose, loanTitle, receiveFormData, total}) => {
  const classes = useStyles();

  return (<Modal
    open={open}
    aria-labelledby="simple-modal-title"
    aria-describedby="simple-modal-description"
    onClose={handleClose}
  >
    <div className={classes.paper}>
      <h2 id="modal-title">Invest to "{loanTitle}"</h2>
      <Formik
        initialValues={{amount: ""}}
        validate={(values) => {
          let errors = {};
          if (!values.amount) {
            errors.amount = "Required";
          }
          if (values.amount > total) {
            errors.amount = "You haven`t enough money" ;
          }
          return errors;
        }}
        onSubmit={(values) => {
          setTimeout(() => {
            receiveFormData(values);
          }, 1000)
        }}
        render={() => (
          <Form>
            <Field
              name="amount"
              label="Amount of investment"
              placeholder="Amount of investment"
              type="number"
              component={TextField}
              fullWidth
              margin="normal"
              variant="outlined"
              required
            />
            <Button
              size="large"
              fullWidth
              variant="contained"
              color="primary"
              type="submit">
              Invest
            </Button>
          </Form>
        )}/>
    </div>
  </Modal>)
};

export default ModalComponent
