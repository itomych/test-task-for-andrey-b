import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    overflowX: "auto",
  },
  table: {
    minWidth: 650,
  },
  row: {
    cursor: "pointer"
  }
}));

const isUpdated = (updated) => {
  return updated ? {"background": "#39e039"} : {"background": "white"}
};

const DataTableComponent = ({loans, addTransfer}) => {
  const classes = useStyles();
  return (<Paper className={classes.root}>
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          <TableCell>Title</TableCell>
          <TableCell align="right">Tranche</TableCell>
          <TableCell align="right">Available</TableCell>
          <TableCell align="right">Annualised return</TableCell>
          <TableCell align="right">Term remaining</TableCell>
          <TableCell align="right">LTV</TableCell>
          <TableCell align="right">Amount</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {loans.map(row => (
          <TableRow
            key={row.id}
            onClick={() => addTransfer(row)}
            style={isUpdated(row.updated)}
            className={classes.row}>
            <TableCell component="th" scope="row">
              {row.title}
            </TableCell>
            <TableCell align="right">{row.tranche}</TableCell>
            <TableCell align="right">{row.available}</TableCell>
            <TableCell align="right">{row.annualised_return}</TableCell>
            <TableCell align="right">{row.term_remaining}</TableCell>
            <TableCell align="right">{row.ltv}</TableCell>
            <TableCell align="right">{row.amount}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </Paper>)
};

export default DataTableComponent;
