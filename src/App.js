import React, { useState, useEffect } from "react";
import "./App.css";
import DataTableComponent from "./components/DataTableComponent";
import ModalComponent from "./components/ModalComponent";
import dataLoans from "./data/current-loans";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";

const initTotal = 1000;

const formatNumber = (num) => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
};

const App = () => {
  const [loans, setLoans] = useState([]);
  const [total, setTotal] = useState(initTotal);
  const [selected, setSelected] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const getData = () => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(dataLoans);
      }, 1000);
    });
  };

  useEffect(() => {
    (async function() {
      let result = await getData();
      setLoans(result.loans);
    })();
  }, []);

  const addTransfer = (loan) => {
    setSelected(loan);
    setModalOpen(true)
  };

  const handleClose = () => {
    setModalOpen(false);
  };

  const receiveFormData = (value) => {
    const totalAmount = total - value.amount;
    setTotal(totalAmount);
    const available = formatNumber(selected.available.replace(",", "") - value.amount);
    const changedLoan = {...selected, available, updated: true };
    const newLoans = loans.map(item => {
      if(item.id === selected.id) {
        item = {...changedLoan};
      }
      return item;
    });
    setLoans(newLoans);
    setSelected(null);
    handleClose();
  };

  return (
    <Container fixed maxWidth="lg">
      <Box m={5} color="text.primary">
        Available amount: {total}
      </Box>
      <Box m={5} color="text.primary">
        <DataTableComponent loans={loans} addTransfer={addTransfer}/>
      </Box>
      <ModalComponent
        open={modalOpen}
        handleClose={handleClose}
        loanTitle={selected && selected.title}
        total={total}
        receiveFormData={receiveFormData}
      />
    </Container>
  );
};

export default App;
